import Vue from "vue";
import VueRouter from "vue-router";
import LoginComponent from "../components/login/Login.vue"
import RegisterComponent from "../components/register/Register.vue"

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "Login",
    component: LoginComponent,
  },
  {
    path: "/register",
    name: "Register",
    component: RegisterComponent,
  }
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
